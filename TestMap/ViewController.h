//
//  ViewController.h
//  TestMap
//
//  Created by Liukaibo on 15/11/19.
//  Copyright © 2015年 www.DerbySoft.com. All rights reserved.
//

#import <UIKit/UIKit.h>
@import CoreLocation;
@import MapKit;
//#import <CoreLocation/CoreLocation.h>
//#import <MapKit/MapKit.h>

@interface ViewController : UIViewController <MKMapViewDelegate, CLLocationManagerDelegate>

@property (nonatomic, strong) CLLocationManager *ifcLocationManager;

@property (nonatomic, strong) CLGeocoder *ifcGeocoder;
@property (nonatomic, strong) MKMapView *ifcMapView;

@property (nonatomic, assign) CLLocationCoordinate2D ifcCoordinate;
@property (nonatomic, assign) MKCoordinateRegion ifcRegion;

@property (nonatomic, strong) MKPinAnnotationView *pinAnnotationView;

@end

