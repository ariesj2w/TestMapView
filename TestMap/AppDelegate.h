//
//  AppDelegate.h
//  TestMap
//
//  Created by Liukaibo on 15/11/19.
//  Copyright © 2015年 www.DerbySoft.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

