//
//  main.m
//  TestMap
//
//  Created by Liukaibo on 15/11/19.
//  Copyright © 2015年 www.DerbySoft.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
