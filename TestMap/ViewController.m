//
//  ViewController.m
//  TestMap
//
//  Created by Liukaibo on 15/11/19.
//  Copyright © 2015年 www.DerbySoft.com. All rights reserved.
//

#import "ViewController.h"

#define SEARCH_KEY @"上海海洋水族馆"


@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initLocationManager];
    
    [self searchIfcresidence];
    
    [self initMapView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - init methods
- (void)initLocationManager {
    self.ifcLocationManager = [[CLLocationManager alloc] init];

    if (![CLLocationManager locationServicesEnabled]) {
        NSLog(@"定位服务不可用");
        return;
    }
    
    // 如果没有用户授权则请求用户授权
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined)
    {
        [self.ifcLocationManager requestWhenInUseAuthorization];
    }
    else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse)
    {
        [self.ifcLocationManager setDelegate:self];
        [self.ifcLocationManager setDesiredAccuracy:kCLLocationAccuracyBest];
        CLLocationDistance distance = 10.0;
        [self.ifcLocationManager setDistanceFilter:distance];
        
        [self.ifcLocationManager startUpdatingLocation];
    }
}


- (void)searchIfcresidence {
    self.ifcGeocoder = [[CLGeocoder alloc] init];
    [self.ifcGeocoder geocodeAddressString:SEARCH_KEY completionHandler:^(NSArray *placemarks, NSError *error) {
        if (error == nil && [placemarks count] > 0) {
            CLPlacemark *placemark = [placemarks objectAtIndex:0];
            self.ifcCoordinate = placemark.location.coordinate;
            
            MKCoordinateSpan span = MKCoordinateSpanMake(0.003, 0.003);
            self.ifcRegion = MKCoordinateRegionMake(self.ifcCoordinate, span);
            [self.ifcMapView setRegion:self.ifcRegion];
            
            MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
            [annotation setCoordinate:self.ifcCoordinate];
            
            NSDictionary *dic = [placemark addressDictionary];
            NSString *strTitle = [dic objectForKey:@"Name"];
            strTitle = (strTitle != nil) ? strTitle : @"";
            
            NSString *strCountry = [dic objectForKey:@"Country"];
            strCountry = (strCountry != nil) ? strCountry : @"";
            
            NSString *strCity = [dic objectForKey:@"City"];
            strCity = (strCity != nil) ? strCity : @"";
            
            NSString *strSubLocality = [dic objectForKey:@"SubLocality"];
            strSubLocality = (strSubLocality != nil) ? strSubLocality : @"";
            
            NSString *strSubtitle = [NSString stringWithFormat:@"%@%@%@", strCountry, strCity, strSubLocality];
            
            [annotation setTitle:strTitle];
            [annotation setSubtitle:strSubtitle];
            
            [self.ifcMapView addAnnotation:annotation];
            [self.ifcMapView selectAnnotation:annotation animated:YES];
        }
    }];
}


- (void)initMapView {
    self.ifcMapView = [[MKMapView alloc] initWithFrame:CGRectMake(0.0f,
                                                                  0.0f,
                                                                  self.view.frame.size.width,
                                                                  self.view.frame.size.height)];
    [self.view addSubview:self.ifcMapView];
    self.ifcMapView.showsUserLocation = YES;
    [self.ifcMapView setDelegate:self];
}

#pragma mark - CoreLocation
//- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
//    CLLocation *location = [locations firstObject];
//    CLLocationCoordinate2D coordinate = location.coordinate;
//    NSLog(@"location = %@, corrdinate.latitude = %f", location, coordinate.latitude);
//    [self.ifcLocationManager stopUpdatingLocation];
//}

#pragma mark - 
- (MKAnnotationView*)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    NSString *reuseIdentifier = @"pinAnnotationView";
    self.pinAnnotationView = (MKPinAnnotationView *)[self.ifcMapView dequeueReusableAnnotationViewWithIdentifier:reuseIdentifier];
    if (self.pinAnnotationView == nil) {
        self.pinAnnotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    }
    
    self.pinAnnotationView.canShowCallout = YES;
    self.pinAnnotationView.draggable = YES;
    self.pinAnnotationView.animatesDrop = YES;
    self.pinAnnotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    
    [self.pinAnnotationView setPinTintColor:[UIColor blueColor]];
    
    return self.pinAnnotationView;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    NSLog(@"btn clicked.");
}



@end
